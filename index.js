// letter s or d

db.users.find({$or:[{firstName:{$regex: 'S', $options: 'i'}}, {lastName:{$regex: 'D', $options:'i'}} ] }, {firstName: 1, lastName: 1, _id:0});

// letter e and their age greater than or equal to 70
db.users.find({department:"HR", age: {$gte: 70}});


// letter e in the first name and age is less than or equal to 30
db.users.find({$and:[{firstName:{$regex:'E', $options:'i'}}, {age: {$lte:30}}]});
